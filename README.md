# Arduino Test App

Basic repository to get started with an arduino project.


### Installation

1. Unzip ```arduino-1.8.5-linux64.tar.xz``` to ```~/programs/arduino/v1.8.5```
2. Clone this repo and ```cd``` into it.
3. ```make install```
4. ```chmod -R 774 src/ vendor/```
5. ```make```
6. [Disco!](https://www.youtube.com/watch?v=dQw4w9WgXcQ) :guitar: :metal: :boom:


### License

This project is open-sourced under the [MIT license](http://opensource.org/licenses/MIT). Enjoy!

