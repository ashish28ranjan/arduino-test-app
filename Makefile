# need to set this for your environment
ARDUINO_DIR = ~/programs/arduino/v1.8.5

.PHONY: src

src:
	$(ARDUINO_DIR)/arduino-builder \
		-compile \
		-verbose \
		-hardware $(ARDUINO_DIR)/hardware \
		-tools $(ARDUINO_DIR)/tools-builder \
		-tools $(ARDUINO_DIR)/hardware/tools/avr \
		-built-in-libraries $(ARDUINO_DIR)/libraries \
		-hardware ./vendor/hardware \
		-libraries ./vendor/libraries \
		-fqbn arduino:avr:uno \
		./src/led.ino

# Run `make install` to install dependencies
install:
	mkdir -p vendor/hardware
	mkdir -p vendor/libraries
	chmod -R 774 src/ vendor/

